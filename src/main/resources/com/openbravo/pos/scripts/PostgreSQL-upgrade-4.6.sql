--fe integration
ALTER TABLE tickets ADD COLUMN fe_Responsecufe text;
ALTER TABLE tickets ADD COLUMN fe_Responseqr text;
ALTER TABLE tickets ADD COLUMN remotestatus integer DEFAULT 0;
UPDATE TICKETS SET remotestatus=1;
-- UPDATE App' version 4.6.3
UPDATE APPLICATIONS SET NAME = $APP_NAME{}, VERSION = $APP_VERSION{} WHERE ID = $APP_ID{};
