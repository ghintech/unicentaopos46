/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghintech.sync;

import com.openbravo.basic.BasicException;
import com.ghintech.sync.customers.CustomerInfoExt;
import com.ghintech.sync.customers.Customer;
import com.ghintech.sync.customers.Location;
import com.google.gson.JsonObject;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.JRootApp;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.idempiere.webservice.client.request.QueryDataRequest;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author egil
 */
public final class SyncCustomers extends Sync {

    int OffSet;
    int Limit;
    QueryDataRequest ws;

    public SyncCustomers(JRootApp app) {
        super(app);
        OffSet = 1;
        Limit = getwsLimit();
        ws = new QueryDataRequest();
        ws.setWebServiceType(getWsCustomerType());
        ws.setLogin(getLogin());
        ws.setLimit(Limit);

    }

    public void getCustomerInfo() {
        String Token = getusertoken();
        String Token2 = getclienttoken(Token);
        JSONObject data = getCustomerData(Token2, 0);
        JSONArray responseJson = data.getJSONArray("records");
        //"page-count": 28,
        //"records-size": 4,
        //"skip-records": 0,
        //"row-count": 111,
        //"array-count": 4,

        if (responseJson.length() == 0) {
            return;
        }
        int page_count = data.getInt("page-count");

        //System.out.println(responseJson);
        //WebServiceConnection client = getClient();
        try {
            for (int page = 0; page < page_count; page++) {
                OffSet = data.getInt("records-size");
                data = getCustomerData(Token2, page * OffSet);
                responseJson = data.getJSONArray("records");
                List<Customer[]> listcustomers = new ArrayList<>();
                System.out.println("page-count: " + data.getInt("page-count"));
                System.out.println("records-size: " + data.getInt("records-size"));
                System.out.println("skip-records: " + data.getInt("skip-records"));
                System.out.println("row-count: " + data.getInt("row-count"));
                System.out.println("array-count: " + data.getInt("array-count"));
                //do {
                /*WindowTabDataResponse response = client.sendRequest(ws);
                
                if (response.getStatus() == WebServiceResponseStatus.Error) {
                    System.out.println(response.getErrorMessage());
                    return;
                } else {
                    System.out.println(response.getErrorMessage());
                    System.out.println("Total rows: " + response.getTotalRows());
                    System.out.println("Num rows: " + response.getNumRows());
                    System.out.println("Start row: " + response.getStartRow());
                    if (response.getNumRows() > 0) {
                        OffSet += Limit;
                    } else {
                        OffSet = 0;
                    }
                 */
                Customer[] customer = new Customer[responseJson.length()];

                for (int i = 0; i < responseJson.length(); i++) {
                    //System.out.println("Row: " + (i + 1));
                    customer[i] = new Customer();

                    if (responseJson.getJSONObject(i).has("CustomerName")) {

                        customer[i].setName(responseJson.getJSONObject(i).get("CustomerName").toString());
                    }
                    if (responseJson.getJSONObject(i).has("Value")) {

                        customer[i].setSearchKey(responseJson.getJSONObject(i).get("Value").toString());
                    }
                    if (responseJson.getJSONObject(i).has("TaxID")) {

                        customer[i].setTaxId(responseJson.getJSONObject(i).get("TaxID").toString());

                    }
                    if (responseJson.getJSONObject(i).has("id")) {

                        customer[i].setId(responseJson.getJSONObject(i).get("id").toString());

                    }
                    if (responseJson.getJSONObject(i).has("Description")) {

                        customer[i].setDescription(responseJson.getJSONObject(i).get("Description").toString());

                    }
                    if (responseJson.getJSONObject(i).has("Address1")) {

                        Location[] loc = new Location[1];
                        loc[0] = new Location();
                        loc[0].setAddress1(responseJson.getJSONObject(i).get("Address1").toString());
                        customer[i].setLocations(loc);

                    }
                    if (responseJson.getJSONObject(i).has("IsActive")) {

                        if (responseJson.getJSONObject(i).get("IsActive").toString().compareTo("Y") == 0) {
                            customer[i].setVisible(Boolean.TRUE);
                        } else {
                            customer[i].setVisible(Boolean.FALSE);
                        }

                    }
                    if (responseJson.getJSONObject(i).has("TotalOpenBalance")) {

                        //customer[i].setMaxdebt(Double.parseDouble(fieldValue));
                        //customer[i].setCurdebt(0.0);
                    }
                    if (responseJson.getJSONObject(i).has("Updated")) {
                        SimpleDateFormat df =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                        df.setTimeZone(TimeZone.getTimeZone("America/Panama"));
                        try {
                            customer[i].setUpdated(df.parse(responseJson.getJSONObject(i).get("Updated").toString()));

                        } catch (ParseException ex) {
                            Logger.getLogger(SyncCustomers.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                    listcustomers.add(customer);
                }
                
                ImportCustomers(listcustomers);
            }
           

        } catch (NumberFormatException e) {
            Logger.getLogger(SyncProducts.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }

    public boolean ImportCustomers(List<Customer[]> listcustomers) {
        for (Customer[] customers : listcustomers) {
            try {

                if (customers == null) {
                    throw new BasicException(AppLocal.getIntString("message.returnnull"));
                }

                if (customers.length > 0) {

                    //dlintegration. syncCustomersBefore();
                    //Add RIF to field Taxid
                    for (Customer customer : customers) {
                        //System.out.println("Registrando Cliente: " + customer.getName());
                        CustomerInfoExt cinfo = new CustomerInfoExt(customer.getId());
                        cinfo.setTaxid(customer.getTaxId());
                        cinfo.setSearchkey(customer.getSearchKey());
                        cinfo.setName(customer.getName());
                        cinfo.setNotes(customer.getDescription());
                        Location loc[] = new Location[1];
                        loc = customer.getLocations();
                        if(loc != null)
                            cinfo.setAddress(loc[0].getAddress1());
                        
                        cinfo.setVisible(customer.getVisible());
                        cinfo.setMaxdebt(customer.getMaxdebt());
                        cinfo.setCurdebt(customer.getCurdebt());
                        cinfo.setUpdated(customer.getUpdated());

                        // TODO: Finish the integration of all fields.
                        dlintegration.syncCustomer(cinfo);
                    }
                    return true;
                }

            } catch (BasicException ex) {
                Logger.getLogger(SyncCustomers.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;

    }

    public String getusertoken() {
        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("userName", getUserName());
            jObj.addProperty("password", getUserPass());
            URL url = new URL(getUrlBase() + "api/v1/auth/tokens");
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jObj.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);

            String p = jsonObject.get("token").toString();
            in.close();
            return p;
        } catch (Exception e) {
            System.out.println("Error de Autentificacion: " + e.getLocalizedMessage());
        }
        return null;

    }

    public String getclienttoken(String usertoken) {
        try {

            JsonObject jObj = new JsonObject();

            jObj.addProperty("roleId", getAD_Role_ID());
            jObj.addProperty("language", getLanguage());
            jObj.addProperty("clientId", getAD_Client_ID());
            jObj.addProperty("organizationId", getAD_Org_ID());
            jObj.addProperty("warehouseId", getM_Warehouse_ID());

            URL url = new URL(getUrlBase() + "api/v1/auth/tokens");
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + usertoken);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("PUT");

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jObj.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);
            String p = jsonObject.get("token").toString();

            in.close();
            conn.disconnect();
            return p;
        } catch (Exception e) {
            System.out.println("Error de datos de cliente: " + e.getLocalizedMessage());
        }
        return "asd";
    }

    public JSONObject getCustomerData(String usertoken, int skip) {
        try {
            StringBuilder full_url = new StringBuilder();
            full_url.append(getUrlBase() + "api/v1/models/C_BPartner_WS_Queue_V?$skip=" + skip + "&");
            //full_url.append("$filter=(WasPrinted eq false;)");
            //full_url.append("$filter=" + URLEncoder.encode("Updated gt now()-interval 1 day", "UTF-8"));

            URL url = new URL(full_url.toString());
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + usertoken);
            conn.setDoOutput(false);
            conn.setDoInput(true);
            conn.setRequestMethod("GET");

            /*OutputStream os = urlConnection.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
		osw.write(param.toString());
		osw.flush();
		osw.close();
		os.close(); */
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);
            //System.out.println(jsonObject);
            in.close();
            conn.disconnect();
            return jsonObject;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }

}
