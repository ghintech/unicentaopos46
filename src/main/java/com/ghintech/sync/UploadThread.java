//    Openbravo POS is a point of sales application designed for touch screens.
//    http://www.openbravo.com/product/pos
//    Copyright (c) 2007 openTrends Solucions i Sistemes, S.L
//    Modified by Openbravo SL on March 22, 2007
//    These modifications are copyright Openbravo SL
//    Author/s: A. Romero 
//    You may contact Openbravo SL at: http://www.openbravo.com
//
//		Contributor: Redhuan D. Oon - ActiveMQ XML string creation for MClient.sendmessage()
//		Please refer to notes at http://red1.org/adempiere/viewtopic.php?f=29&t=1356
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.
package com.ghintech.sync;

import com.cds.sync.SyncGiftCardW;
import com.ghintech.fiscalprint.DataLogicFiscal;
import com.openbravo.basic.BasicException;
import com.ghintech.sync.externalsales.OrderIdentifier;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.openbravo.data.gui.MessageInf;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.DataLogicSystem;
import com.openbravo.pos.forms.JRootApp;
import com.openbravo.pos.payment.JPaymentGiftCardPanel;
import com.openbravo.pos.ticket.ProductInfoExt;
import com.openbravo.pos.ticket.TicketInfo;
import com.openbravo.pos.ticket.TicketLineInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import static java.lang.Thread.sleep;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.validator.EmailValidator;

import org.idempiere.webservice.client.base.DataRow;
import org.json.JSONArray;
import org.json.JSONObject;


public class UploadThread extends Thread {

    private final JRootApp app;

    private final DataLogicIntegration dlintegration;
    private final DataLogicFiscal dlfiscal;
    SyncOrder orders;
    SyncGiftCardW GiftCardW;
    protected DataLogicSystem dlsystem;
    private final Properties erpProperties;
    private final String hostname;

    public UploadThread(JRootApp rootApp) {

        app = rootApp;
        dlintegration = (DataLogicIntegration) app.getBean("com.ghintech.sync.DataLogicIntegration");
        dlfiscal = (DataLogicFiscal) app.getBean("com.ghintech.fiscalprint.DataLogicFiscal");
        orders = new SyncOrder(app);
        GiftCardW = new SyncGiftCardW(app);
        hostname = getHostName();
        dlsystem = (DataLogicSystem) app.getBean("com.openbravo.pos.forms.DataLogicSystem");
        erpProperties = dlsystem.getResourceAsProperties("erp.properties");

    }

    @Override
    public void run() {

        boolean sent = true;
        Double stopLoop;
        int c = 0;
        try {
            //dlintegration.checkTickets();
            if (erpProperties.getProperty("SentAllOrders").equals("Y")) {
                dlintegration.checkTicketsFiscalNumber();
            }

        } catch (BasicException e) {

        }
        while (true) {
            try {

                stopLoop = sent == true ? orders.getWsOrderTypeInterval() : 0.25;

                if (c != 0) {
                    sleep(converter(stopLoop));
                }
                
                System.out.println(exportOrderToERP().getMessageMsg());
                
                if(erpProperties.getProperty("wsGiftCardWrite").compareTo("")==0)
                    System.out.println(exportGiftCardToERP().getMessageMsg());
                
                sent = true;
            } catch (InterruptedException | BasicException ex) {
                Logger.getLogger(UploadThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            c++;
        }

    }

    public long converter(Double min) {
        long millis = (long) (min * 60 * 1000);
        return millis;
    }

    public MessageInf exportOrderToERP() throws BasicException {

        List<TicketInfo> ticketlistSync = dlintegration.getTicketsSyncFE(hostname);
        if (!ticketlistSync.isEmpty()) {
            dlintegration.resetTicketsSyncFE(hostname);
            //return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.sendingorders"));
        }
        //if there is not tickets in process update the list of tickets not sync
        dlintegration.setTicketsInProcessFE(hostname);

        List<TicketInfo> ticketlist = dlintegration.getTicketsSyncFE(hostname);
        for (TicketInfo ticket : ticketlist) {
            ticket.setLines(dlintegration.getTicketLines(ticket.getId()));
            ticket.setPayments(dlintegration.getTicketPayments(ticket.getId()));
        }
        if (ticketlist.isEmpty()) {
            //dlintegration.execTicketUpdate();
            return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.zeroorders"));
        } else {
            int result = transformTicketsJSON(ticketlist, orders);
            if (result != 0) {
                return new MessageInf(MessageInf.SGN_SUCCESS, AppLocal.getIntString("message.syncordersok"), AppLocal.getIntString("message.syncordersinfo") + ticketlist.size());
            } else {
                return new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.syncorderserror"), AppLocal.getIntString("message.syncordersinfo") + ticketlist.size());
            }
        }

    }

    public MessageInf exportOrderToERP(TicketInfo newticket) throws BasicException {

        //List<TicketInfo> ticketlistSync = dlintegration.getTicketsSyncFE(hostname);
        //if (!ticketlistSync.isEmpty()) {
        //    dlintegration.resetTicketsSyncFE(hostname);
        //return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.sendingorders"));
        //}
        //if there is not tickets in process update the list of tickets not sync
        dlintegration.setTicketsInProcessFE(hostname,newticket.getId());
        List<TicketInfo> ticketlist = new ArrayList<>();//dlintegration.getTicketsSyncFE(hostname);
        ticketlist.add(newticket);
        for (TicketInfo ticket : ticketlist) {
            ticket.setLines(dlintegration.getTicketLines(ticket.getId()));
            ticket.setPayments(dlintegration.getTicketPayments(ticket.getId()));
        }
        if (ticketlist.isEmpty()) {
            //dlintegration.execTicketUpdate();
            return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.zeroorders"));
        } else {
            int result = transformTicketsJSON(ticketlist, orders);
            if (result != 0) {
                return new MessageInf(MessageInf.SGN_SUCCESS, AppLocal.getIntString("message.syncordersok"), AppLocal.getIntString("message.syncordersinfo") + ticketlist.size());
            } else {
                return new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.syncorderserror"), AppLocal.getIntString("message.syncordersinfo") + ticketlist.size());
            }
        }

    }

   
    private String getHostName() {
        Properties m_propsconfig = new Properties();
        File file = new File(new File(System.getProperty("user.home")), AppLocal.APP_ID + ".properties");
        try {
            InputStream in;
            in = new FileInputStream(file);
            m_propsconfig.load(in);
            in.close();
        } catch (IOException e) {

        }
        return m_propsconfig.getProperty("machine.hostname");
    }

    public MessageInf exportGiftCardToERP() throws BasicException {

        List<JPaymentGiftCardPanel> listSync = dlintegration.getGiftCardSync(hostname);
        int i = 0;
        if (listSync.size() > 0) {
            dlintegration.resetGiftCardSync(hostname);
        }
        dlintegration.setGiftCardInProcess(hostname);

        List<JPaymentGiftCardPanel> giftcardlist = dlintegration.getGiftCardSync(hostname);

        if (giftcardlist.isEmpty()) {
            dlintegration.execGiftCardUpdate();
            return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.zerogiftcard"));
        } else {
            int result = transformGiftCard(giftcardlist, GiftCardW);
            if (result != 0) {
                return new MessageInf(MessageInf.SGN_SUCCESS, AppLocal.getIntString("message.syncgiftcardok"), AppLocal.getIntString("message.syncordersinfo") + giftcardlist.size());
            } else {
                return new MessageInf(MessageInf.SGN_WARNING, AppLocal.getIntString("message.syncgiftcarerror"), AppLocal.getIntString("message.syncordersinfo") + giftcardlist.size());
            }
        }
    }

    private int transformGiftCard(List<JPaymentGiftCardPanel> giftcardlist, SyncGiftCardW GiftCardW) throws BasicException {
        int imported = 0;

        System.out.println("Cantidad de Gift Card para enviar: " + giftcardlist.size());
        for (int i = 0; i < giftcardlist.size(); i++) {
            if (null != this) {
                JPaymentGiftCardPanel giftcard = giftcardlist.get(i);
                DataRow data = new DataRow();
                data.addField("AD_Client_ID", GiftCardW.getAD_Client_ID());
                data.addField("AD_Org_ID", GiftCardW.getAD_Org_ID());
                data.addField("Serial", giftcard.getSerial());
                data.addField("Amt", giftcard.getAmount());
                data.addField("POS_ID", giftcard.getId());

                System.out.println("Enviando gift card serial número: " + giftcard.getSerial());

                DataRow recordGiftCard = GiftCardW.SendGiftCardW(data);
                Integer giftCardID = recordGiftCard.getField("giftcard_id").getIntValue();
                Date updated = recordGiftCard.getField("updated").getDateValue();

                System.out.println("El resultado fue: " + giftCardID);

                if (giftCardID != 0) {
                    dlintegration.execGiftCardUpdate(giftcard.getId(), "1");
                    JPaymentGiftCardPanel giftcardSync = new JPaymentGiftCardPanel();
                    giftcardSync.setId(giftcard.getId());
                    giftcardSync.setSerial(giftcard.getSerial());
                    giftcardSync.setAmount(giftcard.getAmount());
                    giftcardSync.setGiftCardID(giftCardID);
                    giftcardSync.setUpdated(updated);
                    dlintegration.syncGiftCard(giftcardSync);
                    System.out.println("*************Gift Card Importada: " + giftcard.getSerial() + "*************");
                    imported = 1;
                } else {
                    System.out.println("*************Falló al procesar la Gift Card: " + giftcard.getSerial() + "*************");
                }
            }
        }
        return imported;
    }

    private int transformTicketsJSON(List<TicketInfo> ticketlist, SyncOrder orders) {
        //String result;
        int imported = 0;
        int C_OrderType_ID = 0;

        System.out.println("Cantidad de ticket para enviar: " + ticketlist.size());
        for (int i = 0; i < ticketlist.size(); i++) {
            //f (null != this) {
            try {
                TicketInfo ticket = ticketlist.get(i);

                OrderIdentifier orderid = new OrderIdentifier();
                orderid.setDocumentNo(Integer.toString(ticket.getTicketId()));
                if (ticket.getTicketType() == 0) {

                        C_OrderType_ID = orders.getC_DocType_ID();
                    } else {

                        C_OrderType_ID = orders.getC_DocTypeRefund_ID();
                    }
                JSONObject corder = orders.checkOrder(Integer.toString(ticket.getTicketId()),C_OrderType_ID);
                
                
                JSONArray corder_response=null;
                if(corder!=null){
                    corder_response = corder.getJSONArray("records");
                        if (corder_response.length() != 0) {
                            JSONObject fe = orders.downloadFE(corder_response.getJSONObject(0).get("id").toString());
                
                            String FE_ResponseCUFE = "";
                            String FE_ResponseQR = "";
                            if (fe != null) {
                                JSONArray fe_response = fe.getJSONArray("records");

                                if (fe_response.length() > 0) {
                                    FE_ResponseCUFE = fe_response.getJSONObject(0).get("FE_ResponseCUFE").toString();
                                    FE_ResponseQR = fe_response.getJSONObject(0).get("FE_ResponseQR").toString();
                                }
                            }
                            try {
                                dlintegration.execTicketUpdateFE(ticket.getId(), "1", FE_ResponseCUFE, URLEncoder.encode(FE_ResponseQR, "UTF-8"));
                            } catch (UnsupportedEncodingException ex) {
                                Logger.getLogger(UploadThread.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            System.out.println("*************Orden Importada: " + ticket.getTicketId() + "*************");
                            imported++;
                        }  
                else{
                
                    Calendar datenew = Calendar.getInstance();
                    datenew.setTime(ticket.getDate());
                    orderid.setDateNew(datenew);

                    if (ticket.getCustomerId() != null) {
                        ticket.setCustomer(dlintegration.getTicketCustomer(ticket.getId()));
                    }
                    

                    ////////////////////////////////////////////////////////////
                    /*
                        JsonObject jObjBPARTNERLOCATION = new JsonObject();	
                        jObjBPARTNERLOCATION.addProperty("Name", "Panama");
                        jObjBPARTNERLOCATION.addProperty("C_Location_ID", 1000159);
                     */
                    JsonObject jObjBPARTNERUSER = new JsonObject();
                    jObjBPARTNERUSER.addProperty("Name", ticket.getCustomer().getFirstname() != null ? ticket.getCustomer().getFirstname() : ticket.getCustomer().getName());
                    if(ticket.getCustomer().getEmail()!=null){
                        if(isValidEmail(ticket.getCustomer().getEmail().replaceAll(" ", "")))
                            jObjBPARTNERUSER.addProperty("EMail", ticket.getCustomer().getEmail().replaceAll(" ", ""));
                    }
                    JsonArray jArrayBPARTNERUSER = new JsonArray();
                    jArrayBPARTNERUSER.add(jObjBPARTNERUSER);

                    JsonObject jObjLocation = new JsonObject();
                    jObjLocation.addProperty("identifier", "Panama");
                    jObjLocation.addProperty("tableName", "C_Location");
                    JsonObject jObjBPARTNERLOCATION = new JsonObject();
                    jObjBPARTNERLOCATION.add("C_Location_ID", jObjLocation);
                    jObjBPARTNERLOCATION.addProperty("Name", "Panama");
                    JsonArray jArrayBPARTNERLOCATION = new JsonArray();
                    jArrayBPARTNERLOCATION.add(jObjBPARTNERLOCATION);

                    JsonObject jObjLCO_TaxIdType = new JsonObject();
                    jObjLCO_TaxIdType.addProperty("identifier", "N");
                    jObjLCO_TaxIdType.addProperty("tableName", "LCO_TaxIdType");
                    ///////////////////////////////////////////////////////////////////////////////////

                    JsonObject jObjBPARTNER = new JsonObject();
                    jObjBPARTNER.addProperty("value", ticket.getCustomer().getSearchkey());
                    jObjBPARTNER.addProperty("name", ticket.getCustomer().getName());
                    jObjBPARTNER.addProperty("TaxID", ticket.getCustomer().getTaxid()!=null?ticket.getCustomer().getTaxid():"");

                    jObjBPARTNER.add("AD_User", jArrayBPARTNERUSER);

                    jObjBPARTNER.add("C_BPartner_Location", jArrayBPARTNERLOCATION);
                    jObjBPARTNER.add("LCO_TaxIdType_ID", jObjLCO_TaxIdType);
                    jObjBPARTNER.addProperty("TipoClienteFE", "02"); ///// Tipo Cliente - TipoClienteFE - id - 02 = Consumidor final

                    ////////////////////////////////////////////////////////////
                    JSONObject bpartner = orders.checkBPartner(ticket.getCustomer().getSearchkey());
                    JSONArray bpartner_response=null;
                    if(bpartner!=null)
                        bpartner_response = bpartner.getJSONArray("records");
                    else
                        return 0;
                    String bpartner_response_id = null;
                    if (bpartner_response.length() == 0) {
                        bpartner = orders.createBPartner(jObjBPARTNER);
                        bpartner_response_id = bpartner.get("id").toString();
                    } else {
                        bpartner_response_id = bpartner_response.getJSONObject(0).get("id").toString();
                    }

                    System.out.println(bpartner_response_id);
                    //JSONArray responseJson=bpartner.getJSONArray("records");

                    //JsonObject jObjDOCTYPE = new JsonObject();	
                    //jObjDOCTYPE.addProperty("identifier", 1000066);
                    JsonArray jArrayLines = new JsonArray();

                    for (int j = 0; j < ticket.getLines().size(); j++) {
                        JsonObject jObjLine = new JsonObject();

                        TicketLineInfo line = ticket.getLines().get(j);
                        ////////////////////////////////////////////////////////////
                        ProductInfoExt productinfo = dlintegration.getProductInfo(line.getProductID());
                        if(productinfo!=null){
                            JSONObject mproduct = orders.checkMProduct(productinfo.getReference());
                            JSONArray mproduct_response = mproduct.getJSONArray("records");

                            if (mproduct_response.length() != 0) {

                                jObjLine.addProperty("M_Product_ID", Integer.valueOf(mproduct_response.getJSONObject(0).get("id").toString()));
                            } else {
                                jObjLine.addProperty("C_UOM_ID", 100);
                            }
                        }else {
                                jObjLine.addProperty("C_UOM_ID", 100);
                            }

                        jObjLine.addProperty("Description", line.getProductName());
                        jObjLine.addProperty("QtyEntered", Double.toString(Math.abs(line.getMultiply())));
                        jObjLine.addProperty("PriceEntered", Double.toString(line.getPrice()));
                        JsonObject jObjC_Tax_ID = new JsonObject();
                        jObjC_Tax_ID.addProperty("identifier", line.getTaxInfo().getName());
                        jObjC_Tax_ID.addProperty("tableName", "C_Tax");
                        jObjLine.add("C_Tax_ID", jObjC_Tax_ID);

                        jArrayLines.add(jObjLine);

                    }

                    JsonObject jObj = new JsonObject();

                    jObj.addProperty("isSOTrx", true);
                    jObj.addProperty("C_DocTypeTarget_ID", C_OrderType_ID);

                    jObj.addProperty("C_BPartner_ID", Integer.valueOf(bpartner_response_id));
                    jObj.addProperty("deliveryViaRule", "P");
                    jObj.add("order-line", jArrayLines);
                    jObj.addProperty("doc-action", "CO");
                    JsonObject jObjAD_User_ID = new JsonObject();
                    jObjAD_User_ID.addProperty("identifier", ticket.getUser().getName());
                    jObjAD_User_ID.addProperty("tableName", "AD_User");
                    jObj.add("SalesRep_ID", jObjAD_User_ID);
                    //jObj.addProperty("SalesRep_ID",Integer.valueOf(ticket.getUser().getId()));
                    jObj.addProperty("M_PriceList_ID", orders.getM_PriceList_Version_ID());
                    jObj.addProperty("DocumentNo", ticket.getTicketId());
                    jObj.addProperty("POReference", ticket.getTicketId());
                    ///// Enviar Orden 
                    System.out.println(jObj);
                    JSONObject resultorder = orders.sendOrder(jObj);

                    if(resultorder != null){
                         JSONObject fe = orders.downloadFE(resultorder.get("id").toString());

                        String FE_ResponseCUFE = "";
                        String FE_ResponseQR = "";
                        if (fe != null) {
                            JSONArray fe_response = fe.getJSONArray("records");

                            if (fe_response.length() > 0) {
                                FE_ResponseCUFE = fe_response.getJSONObject(0).get("FE_ResponseCUFE").toString();
                                FE_ResponseQR = fe_response.getJSONObject(0).get("FE_ResponseQR").toString();
                            }
                        }
                    //if (resultorder != null) {
                        System.out.println("*************Orden Importada: " + ticket.getTicketId() + "*************");
                        try {
                            dlintegration.execTicketUpdateFE(ticket.getId(), "1", FE_ResponseCUFE, URLEncoder.encode(FE_ResponseQR, "UTF-8"));
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(UploadThread.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        imported++;
                        //consultamos la factura 
                    } else {
                        System.out.println("*************Falló al procesar orden: " + ticket.getTicketId() + "*************");
                        dlintegration.execTicketUpdateFE(ticket.getId(), "0");
                    }
                }
                }

            } catch (BasicException ex) {
                Logger.getLogger(SyncOrderThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            //}
        }
        return imported;
    }
    public static boolean isValidEmail(String email) {
       // create the EmailValidator instance
       EmailValidator validator = EmailValidator.getInstance();
       // check for valid email addresses using isValid method
       return validator.isValid(email);
   }
}
