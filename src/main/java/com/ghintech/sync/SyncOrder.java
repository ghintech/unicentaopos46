/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghintech.sync;

import com.google.gson.JsonObject;
import com.openbravo.pos.forms.JRootApp;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.apache.commons.io.IOUtils;
import org.idempiere.webservice.client.base.DataRow;
import org.idempiere.webservice.client.base.Enums;
import org.idempiere.webservice.client.base.ParamValues;
import org.idempiere.webservice.client.exceptions.WebServiceException;
import org.idempiere.webservice.client.net.WebServiceConnection;
import org.idempiere.webservice.client.request.CompositeOperationRequest;
import org.idempiere.webservice.client.request.CreateDataRequest;
import org.idempiere.webservice.client.request.QueryDataRequest;
import org.idempiere.webservice.client.request.RunProcessRequest;
import org.idempiere.webservice.client.response.CompositeResponse;
import org.idempiere.webservice.client.response.RunProcessResponse;
import org.idempiere.webservice.client.response.WindowTabDataResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author egil
 */
public final class SyncOrder extends Sync {

    public SyncOrder(JRootApp app) {
        super(app);
    }

    public int SendOrders(DataRow data) {
        int Record_ID=0;
        CompositeOperationRequest compositeOperation = new CompositeOperationRequest();
        compositeOperation.setLogin(getLogin());
        compositeOperation.setWebServiceType(getwsCompositeOrderType());        
        CreateDataRequest ws = new CreateDataRequest();        
        ws.setWebServiceType(getWsOrderType());
        ws.setLogin(getLogin());       
        ws.setDataRow(data);
        compositeOperation.addOperation(ws);
        try {
            WebServiceConnection client = getClient();
            CompositeResponse response = client.sendRequest(compositeOperation);
            if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {
                System.out.println(response.getErrorMessage());
                return 0;
            }
            //now i need to get the ID of the record
           QueryDataRequest invoice = new QueryDataRequest();
           invoice.setWebServiceType(getwsCurrentOrder());
           invoice.setLogin(getLogin());
           invoice.setLimit(1);    
           DataRow datainvoice = new DataRow();
           datainvoice.addField("DocumentNo", data.getField("DocumentNo").getValue());
           datainvoice.addField("OPOS_line", data.getField("OPOS_line").getValue());
           datainvoice.addField("AD_Org_ID", data.getField("AD_Org_ID").getValue());
           datainvoice.addField("C_DocType_ID", data.getField("C_DocType_ID").getValue());
           invoice.setDataRow(datainvoice);
           WebServiceConnection clienti = getClient();
           WindowTabDataResponse responsei = clienti.sendRequest(invoice);  
           if (responsei.getStatus() == Enums.WebServiceResponseStatus.Error) {
               System.out.println(responsei.getErrorMessage());
		return 0;
           } else {
                Record_ID = responsei.getDataSet().getRow(0).getFields().get(0).getIntValue();    
           }                        
        } catch (WebServiceException | NumberFormatException e) {
            System.err.println(e.getMessage());
        }
        return Record_ID;
    }

    public String processOrder(int ticketid){
        String Record_ID="";
        ParamValues data = new ParamValues();
        data.addField("AD_Client_ID",this.getAD_Client_ID());
        data.addField("AD_Org_ID",this.getAD_Org_ID());
        data.addField("DocumentNo", Integer.toString(ticketid));
        data.addField("DocAction", "CO");
        RunProcessRequest ws = new RunProcessRequest();
        
        ws.setWebServiceType(getwsImportOrderType());
        ws.setLogin(getLogin());
        
        ws.setParamValues(data);
        WebServiceConnection client = getClient();

        try {
            RunProcessResponse response;
            response = client.sendRequest(ws);
            if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {
                System.out.println(response.getErrorMessage());
            } else{
                Record_ID = response.getSummary();
            
            }
        } catch (WebServiceException | NumberFormatException e) {
        }
        return Record_ID;
    }    
    public String processOrder(int ticketid,int C_DocType_ID){
        String Record_ID="";
        ParamValues data = new ParamValues();
        data.addField("AD_Client_ID",this.getAD_Client_ID());
        data.addField("AD_Org_ID",this.getAD_Org_ID());
        data.addField("DocumentNo", Integer.toString(ticketid));
        data.addField("DocAction", "CO");
        data.addField("C_DocType_ID", C_DocType_ID);
        RunProcessRequest ws = new RunProcessRequest();
        
        ws.setWebServiceType(getwsImportOrderType());
        ws.setLogin(getLogin());
        
        ws.setParamValues(data);
        WebServiceConnection client = getClient();

        try {
            RunProcessResponse response;
            response = client.sendRequest(ws);
            if (response.getStatus() == Enums.WebServiceResponseStatus.Error) {
                System.out.println(response.getErrorMessage());
            } else{
                Record_ID = response.getSummary();
            
            }
        } catch (WebServiceException | NumberFormatException e) {
        }
        return Record_ID;
    }
    public String getusertoken()
	{
		try 
		{
			JsonObject jObj = new JsonObject();
			jObj.addProperty("userName", getUserName());
			jObj.addProperty("password", getUserPass());	
			URL url = new URL(getUrlBase()+"api/v1/auth/tokens");
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;

			conn.setConnectTimeout(5000);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");

			OutputStream os = urlConnection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
			osw.write(jObj.toString());
			osw.flush();
			osw.close();
			os.close(); 
			conn.connect();

			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);

			String p = jsonObject.get("token").toString(); 
			in.close();
			return p;
		}
		catch(IOException | JSONException e)
		{
			System.out.println("Error de Autentificacion: "+e.getLocalizedMessage());
		}
		return null;

	}

	public  String getclienttoken(String usertoken) 
	{
		try 
		{

			JsonObject jObj = new JsonObject();		
			/*jObj.addProperty("roleId", getAD_Role_ID());
		jObj.addProperty("language",getLanguage());
		jObj.addProperty("clientId",getAD_Client_ID());
		jObj.addProperty("organizationId",getAD_Org_ID());
		jObj.addProperty("warehouseId",getM_Warehouse_ID());*/
			//Valores para probar
			jObj.addProperty("roleId", getAD_Role_ID());
			jObj.addProperty("language","en_US");
			jObj.addProperty("clientId",getAD_Client_ID());
			jObj.addProperty("organizationId",getAD_Org_ID());
			jObj.addProperty("warehouseId",getM_Warehouse_ID());

			URL url = new URL(getUrlBase()+"api/v1/auth/tokens");
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;

			conn.setConnectTimeout(5000);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+usertoken);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("PUT");

			OutputStream os = urlConnection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
			osw.write(jObj.toString());
			osw.flush();
			osw.close();
			os.close(); 
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			String p = jsonObject.get("token").toString(); 

			in.close();
			conn.disconnect();
			return p;
		}
		catch(IOException | JSONException e)
		{
			System.out.println("Error de datos de cliente: "+e.getLocalizedMessage());
		}
		return "asd";
	}
        public  JSONObject sendOrder(JsonObject order) {
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
			StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/windows/sales-order");
			//full_url.append("$filter=(WasPrinted eq false;)");
			//full_url.append("$filter="+URLEncoder.encode("M_Warehouse_ID eq "+getM_Warehouse_ID(), "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");

			OutputStream os = urlConnection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
			System.out.println(order.toString());
                        osw.write(order.toString());
			osw.flush();
			osw.close();
			os.close(); 
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                        System.out.println(result);
			return jsonObject;
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
                        return null;
		}
		//return null;
	}
        public  JSONObject createBPartner(JsonObject bpartner) {
            /*
            {   
    "value": "V13510900",
    "name":"EDUARDO GIL",
     "AD_User": [
         {             
            "Name":"USER EDUARDO"             
         }
     ],
     "C_Location": [
         {
             "Address1": "Calle 21 DONDE LOS ",
             "C_Country_ID":274
         }
     ],
     "C_BPartner_Location": [
         {
        "C_Location_ID": {
                            "identifier": "Panama",
                             "tableName": "C_Location"
      },
      "Name":"Panama X"
         }
     ]     
}
            
            
            */
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    
                        
			StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/C_BPartner");
			//full_url.append("$filter=(WasPrinted eq false;)");
			//full_url.append("$filter="+URLEncoder.encode("M_Warehouse_ID eq "+getM_Warehouse_ID(), "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");

			OutputStream os = urlConnection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
			osw.write(bpartner.toString());
			osw.flush();
			osw.close();
			os.close(); 
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                        
                
                        System.out.print(jsonObject);
                        return jsonObject;
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
            
        
        public  JSONObject checkBPartner(String bpartner_value) {
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/C_BPartner?");
			//full_url.append("$filter=(WasPrinted eq false;)");
			full_url.append("$filter="+URLEncoder.encode("Value eq '"+bpartner_value+"'", "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");

			
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			//System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                            return jsonObject;
                        
                        
			 
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
        
        public  JSONObject downloadFE(String C_Order_ID) {
            
            JSONObject invoice=findInvoice(C_Order_ID);
            JSONArray invoice_response=invoice.getJSONArray("records");
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/FE_InvoiceResponseLog?");
			//full_url.append("$filter=(WasPrinted eq false;)");
			full_url.append("$filter="+URLEncoder.encode("FE_ResponseCode eq '200' and C_Invoice_ID eq "+invoice_response.getJSONObject(0).get("id").toString(), "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");

			
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			//System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                            return jsonObject;
                        
                        
			 
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
        public  JSONObject findInvoice(String C_Order_ID) {
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/C_Invoice?");
			//full_url.append("$filter=(WasPrinted eq false;)");
			full_url.append("$filter="+URLEncoder.encode("C_Order_ID eq "+C_Order_ID, "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");

			
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			//System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                            return jsonObject;
                        
                        
			 
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
        public  JSONObject checkMProduct(String mproduct_value) {
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/M_Product?");
			//full_url.append("$filter=(WasPrinted eq false;)");
			full_url.append("$filter="+URLEncoder.encode("Value eq '"+mproduct_value+"'", "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");

			
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			//System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                            return jsonObject;
                        
                        
			 
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
        public  JSONObject checkOrder(String order_documentno, int C_OrderType_ID) {
            String Token = getusertoken();
            String Token2 = getclienttoken(Token);
		try 
		{
                    StringBuilder full_url = new StringBuilder();
			full_url.append(getUrlBase()+"api/v1/models/C_Order?");
			//full_url.append("$filter=(WasPrinted eq false;)");
			full_url.append("$filter="+URLEncoder.encode("Documentno eq '"+order_documentno+"' AND AD_Org_ID eq "+this.getAD_Org_ID()+" AND C_DocTypeTarget_ID eq "+C_OrderType_ID, "UTF-8"));		

			URL url = new URL(full_url.toString());
			URLConnection urlConnection = url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) urlConnection;
			
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization","Bearer "+Token2);
			conn.setDoOutput(false);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");

			
			conn.connect();



			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			JSONObject jsonObject = new JSONObject(result);
			//System.out.println(jsonObject);
			in.close();
			conn.disconnect();
                            return jsonObject;
                        
                        
			 
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return null;
	}
}
