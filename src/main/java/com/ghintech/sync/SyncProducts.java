/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghintech.sync;

import com.openbravo.basic.BasicException;
import com.ghintech.sync.externalsales.Category;
import com.ghintech.sync.externalsales.CategoryInfo2;
import com.ghintech.sync.externalsales.Product;
import com.ghintech.sync.externalsales.ProductInfoExt2;
import com.ghintech.sync.externalsales.ProductPlus;
import com.ghintech.sync.externalsales.Tax;
import com.google.gson.JsonObject;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.JRootApp;
import com.openbravo.pos.inventory.MovementReason;
import com.openbravo.pos.inventory.TaxCategoryInfo;
import com.openbravo.pos.ticket.CategoryInfo;
import com.openbravo.pos.ticket.ProductInfoExt;
import com.openbravo.pos.ticket.TaxInfo;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.IOUtils;
import org.idempiere.webservice.client.base.Enums.WebServiceResponseStatus;
import org.idempiere.webservice.client.base.Field;
import org.idempiere.webservice.client.exceptions.WebServiceException;
import org.idempiere.webservice.client.net.WebServiceConnection;
import org.idempiere.webservice.client.request.QueryDataRequest;
import org.idempiere.webservice.client.response.WindowTabDataResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author egil
 */
public final class SyncProducts extends Sync {

    int OffSet;
    int Limit;
    QueryDataRequest ws;
    String IsNotSold = "";

    public SyncProducts(JRootApp app) {
        super(app);
        OffSet = 1;
        Limit = getwsLimit();
        ws = new QueryDataRequest();
        ws.setWebServiceType(getWsProductType());
        ws.setLogin(getLogin());
        ws.setLimit(Limit);

    }

    public void getProductInfo() throws MalformedURLException, IOException {
        String Token = getusertoken();
        String Token2 = getclienttoken(Token);
        JSONObject data = getProductData(Token2, 0);
        JSONArray responseJson = data.getJSONArray("records");
        //"page-count": 28,
        //"records-size": 4,
        //"skip-records": 0,
        //"row-count": 111,
        //"array-count": 4,

        if (responseJson.length() == 0) {
            return;
        }
        int page_count = data.getInt("page-count");

        //System.out.println(responseJson);
        //WebServiceConnection client = getClient();
        try {
            for (int page = 0; page < page_count; page++) {
                OffSet = data.getInt("records-size");
                data = getProductData(Token2, page * OffSet);
                responseJson = data.getJSONArray("records");
                List<ProductPlus[]> listproducts = new ArrayList<>();
                System.out.println("page-count: " + data.getInt("page-count"));
                System.out.println("records-size: " + data.getInt("records-size"));
                System.out.println("skip-records: " + data.getInt("skip-records"));
                System.out.println("row-count: " + data.getInt("row-count"));
                System.out.println("array-count: " + data.getInt("array-count"));
                //do {
                /*WindowTabDataResponse response = client.sendRequest(ws);
                
                if (response.getStatus() == WebServiceResponseStatus.Error) {
                    System.out.println(response.getErrorMessage());
                    return;
                } else {
                    System.out.println(response.getErrorMessage());
                    System.out.println("Total rows: " + response.getTotalRows());
                    System.out.println("Num rows: " + response.getNumRows());
                    System.out.println("Start row: " + response.getStartRow());
                    if (response.getNumRows() > 0) {
                        OffSet += Limit;
                    } else {
                        OffSet = 0;
                    }
                 */
                ProductPlus[] product = new ProductPlus[responseJson.length()];
                //System.out.println("Row qty: " + responseJson.length());
                for (int i = 0; i < responseJson.length(); i++) {

                    //System.out.println("Row: " + (i + 1));
                    product[i] = new ProductPlus();
                    Category cate = new Category();
                    Tax newtax = new Tax();
                    if (responseJson.getJSONObject(i).has("POSLocatorName")) {
                        product[i].setLocation_name(responseJson.getJSONObject(i).get("POSLocatorName").toString());
                    }

                    //Field field = response.getDataSet().getRow(i).getField("POSLocatorName");
                    if (responseJson.getJSONObject(i).has("id")) {
                        product[i].setId(responseJson.getJSONObject(i).get("id").toString());
                    }
                    //Field M_Product_ID = response.getDataSet().getRow(i).getField("M_Product_ID");

                    //product[i].setLocation_name(String.valueOf(field.getValue()));
                    if (responseJson.getJSONObject(i).has("M_Locator_ID")) {
                        product[i].setLocation_id(responseJson.getJSONObject(i).getJSONObject("M_Locator_ID").get("id").toString());
                    }
                    //field = response.getDataSet().getRow(i).getField("M_Locator_ID");

                    if (responseJson.getJSONObject(i).has("ProductValue")) {
                        product[i].setReference(responseJson.getJSONObject(i).get("ProductValue").toString());
                    }

                    //field = response.getDataSet().getRow(i).getField("ProductValue");
                    if (responseJson.getJSONObject(i).has("ProductName")) {
                        product[i].setName(responseJson.getJSONObject(i).get("ProductName").toString());
                    }
                    //field = response.getDataSet().getRow(i).getField("ProductName");
                    //product[i].setName(String.valueOf(field.getValue()));

                    if (responseJson.getJSONObject(i).has("QtyOnHand")) {
                        product[i].setQtyonhand(Double.valueOf(responseJson.getJSONObject(i).get("QtyOnHand").toString()));
                    }

                    //field = response.getDataSet().getRow(i).getField("QtyOnHand");
                    //product[i].setQtyonhand(Double.parseDouble(String.valueOf(field.getValue())));
                    if (responseJson.getJSONObject(i).has("M_Product_Category_ID")) {
                        //field = response.getDataSet().getRow(i).getField("M_Product_Category_ID");
                        cate.setId(responseJson.getJSONObject(i).getJSONObject("M_Product_Category_ID").get("id").toString());
                        cate.setName(responseJson.getJSONObject(i).get("CategoryName").toString());

                        if (cate.getName() != null) {
                            product[i].setCategory(cate);
                        }
                    }

                    //field = response.getDataSet().getRow(i).getField("CategoryName");
                    //cate.setName(String.valueOf(field.getValue()));
                    //if (cate.getName() != null) {
                    //    product[i].setCategory(cate);
                    //}
                    //field = response.getDataSet().getRow(i).getField("M_Product_ID");
                    //product[i].setId(String.valueOf(field.getValue()));
                    if (responseJson.getJSONObject(i).has("C_Tax_ID")) {

                        //field = response.getDataSet().getRow(i).getField("C_Tax_ID");
                        newtax.setId(responseJson.getJSONObject(i).getJSONObject("C_Tax_ID").get("id").toString());
                        newtax.setPercentage(Double.parseDouble(String.valueOf(responseJson.getJSONObject(i).get("TaxRate").toString())));
                        newtax.setName(String.valueOf(responseJson.getJSONObject(i).get("TaxName").toString()));
                        if (newtax.getName() != null) {
                            product[i].setTax(newtax);
                        }

                    }

                    if (responseJson.getJSONObject(i).has("UPC")) {
                        product[i].setEan(responseJson.getJSONObject(i).get("UPC").toString());
                    }
                    //field = response.getDataSet().getRow(i).getField("UPC");
                    //product[i].setEan(String.valueOf(field.getValue()));

                    if (responseJson.getJSONObject(i).has("PriceList")) {
                        product[i].setListPrice(Double.parseDouble(responseJson.getJSONObject(i).get("PriceList").toString()));
                    }

                    //field = response.getDataSet().getRow(i).getField("PriceList");
                    //product[i].setListPrice(Double.parseDouble(String.valueOf(field.getValue())));
                    if (responseJson.getJSONObject(i).has("PriceLimit")) {
                        product[i].setPurchasePrice(Double.parseDouble(responseJson.getJSONObject(i).get("PriceLimit").toString()));
                    }

                    //field = response.getDataSet().getRow(i).getField("PriceLimit");
                    //product[i].setPurchasePrice(Double.parseDouble(String.valueOf(field.getValue())));
                    //if (responseJson.getJSONObject(i).has("ImageURL")) {
                    //field = response.getDataSet().getRow(i).getField("ImageURL");
                    //    if (responseJson.getJSONObject(i).get("ImageURL").toString() != null && responseJson.getJSONObject(i).get("ImageURL").toString().compareTo("") != 0) {
                    //        product[i].setImageUrl(responseJson.getJSONObject(i).get("ImageURL").toString());
                    //        URL imageURL = new URL(responseJson.getJSONObject(i).get("ImageURL").toString());
                    //        BufferedImage img = ImageIO.read(imageURL);
                    //        product[i].setImage(img);
                    //    }
                    //}
                    if (responseJson.getJSONObject(i).has("Updated")) {
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                        df.setTimeZone(TimeZone.getTimeZone("America/Panama"));
                        try {
                            product[i].setUpdated(df.parse(responseJson.getJSONObject(i).get("Updated").toString()));
                        } catch (ParseException ex) {
                            Logger.getLogger(SyncCustomers.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    /*if(responseJson.getJSONObject(i).has("Updated")){
                            //field = response.getDataSet().getRow(i).getField("Updated");
                            try {
                                product[i].setUpdated(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(responseJson.getJSONObject(i).get("Updated").toString()));

                            } catch (ParseException ex) {
                                Logger.getLogger(SyncProducts.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }*/
                    if (responseJson.getJSONObject(i).has("M_Product_Category_Parent_ID")) {
                        //field = response.getDataSet().getRow(i).getField("M_Product_Category_Parent_ID");
                        if (responseJson.getJSONObject(i).get("M_Product_Category_Parent_ID").toString() != null && responseJson.getJSONObject(i).get("M_Product_Category_Parent_ID").toString().compareTo("") != 0) {
                            product[i].getCategory().setParentID(responseJson.getJSONObject(i).getJSONObject("M_Product_Category_Parent_ID").get("id").toString());
                                                                 
                        }
                    }
                    if (responseJson.getJSONObject(i).has("ParentCategoryName")) {
                        //field = response.getDataSet().getRow(i).getField("M_Product_Category_Parent_ID");
                        if (responseJson.getJSONObject(i).get("ParentCategoryName").toString() != null && responseJson.getJSONObject(i).get("ParentCategoryName").toString().compareTo("") != 0) {
                            product[i].getCategory().setParentName(responseJson.getJSONObject(i).get("ParentCategoryName").toString());
                        }
                    }
                    //field = response.getDataSet().getRow(i).getField("ParentCategoryName");
                    //if (String.valueOf(field.getValue()) != null && String.valueOf(field.getValue()).compareTo("") != 0) {
                    //    product[i].getCategory().setParentName(String.valueOf(field.getValue()));
                    //}
                    if (responseJson.getJSONObject(i).has("UOMType")) {
                        if (responseJson.getJSONObject(i).get("UOMType").toString() != null && responseJson.getJSONObject(i).get("UOMType").toString().compareTo("") != 0) {
                            if ("WE".equals(responseJson.getJSONObject(i).get("UOMType").toString())) {
                                product[i].setisScale("Y");
                            }
                        }
                    }
                    //field = response.getDataSet().getRow(i).getField("UOMType");
                    //if (String.valueOf(field.getValue()) != null && !"".equals(String.valueOf(field.getValue()))) {
                    //    if ("WE".equals(String.valueOf(field.getValue()))) {
                    //        product[i].setisScale("Y");
                    //    }
                    //}
                    if (responseJson.getJSONObject(i).has("ProductType")) {
                        
                        if (responseJson.getJSONObject(i).getJSONObject("ProductType").get("id").toString().compareTo("S") == 0) {
                            product[i].setIsService(true);
                        }
                    }

                    if (responseJson.getJSONObject(i).has("IsSold")) {
                        //field = response.getDataSet().getRow(i).getField("IsSold");
                        if (responseJson.getJSONObject(i).get("IsSold").toString().compareTo("N") == 0) {
                            if (IsNotSold.compareTo("") == 0) {
                                IsNotSold = "'" + responseJson.getJSONObject(i).get("ProductValue").toString() + "'";
                            } else {
                                IsNotSold = IsNotSold + ",'" + responseJson.getJSONObject(i).get("ProductValue").toString() + "'";
                            }
                        }
                    }

                }
                listproducts.add(product);
                System.out.println("Productos a procesar" + listproducts.size());
                ImportProducts(listproducts);
            }
            //}
            //ws.setOffset(OffSet);
            //} while (OffSet != 0);

        } catch (NumberFormatException e) {
            Logger.getLogger(SyncProducts.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void ImportProducts(List<ProductPlus[]> listproducts) {

        try {
            for (ProductPlus[] products : listproducts) {
                if (products == null) {
                    throw new BasicException(AppLocal.getIntString("message.returnnull"));
                }
                if (products.length > 0) {
                    dlintegration.syncProductsBefore();
                    Date now = new Date();
                    //System.out.println("Cantidad de productos" + products.length);
                    for (Product product : products) {
                        //System.out.println("Registering Product: " + product.getName() + " updated: " + product.getUpdated());
                        // Synchonization of taxcategories
                        TaxCategoryInfo tc = new TaxCategoryInfo(product.getTax().getId(), product.getTax().getName());
                        dlintegration.syncTaxCategory(tc);
                        TaxCategoryInfo tcaux = dlintegration.getTaxCategoryInfoByName(tc.getName());
                        // Synchonization of taxes
                        TaxInfo t = new TaxInfo(
                                product.getTax().getId(),
                                product.getTax().getName(),
                                tcaux.getID(),
                                //new Date(Long.MIN_VALUE),
                                null,
                                null,
                                product.getTax().getPercentage() / 100,
                                false,
                                0);
                        dlintegration.syncTax(t);
                        // Synchonization of categories
                        CategoryInfo2 c = new CategoryInfo2(product.getCategory().getId(), product.getCategory().getName(), null, null, null);
                        if (product.getCategory().getParentID() != null) {
                            c.setParentID(product.getCategory().getParentID());
                            c.setParentName(product.getCategory().getParentName());
                            dlintegration.syncParentCategory(c);
                        }
                        dlintegration.syncCategory(c);
                        CategoryInfo caux = dlintegration.getCategoryInfoByName(c.getName());
                        // Synchonization of products
                        ProductInfoExt2 p = new ProductInfoExt2();
                        p.setID(product.getId());
                        p.setReference(product.getReference());
                        p.setCode(product.getEan() == null || product.getEan().equals("") ? product.getReference() : product.getEan());
                        // String auxname=product.getName().replaceAll("%(?![0-9a-fA-F]{2})", "%25");
                        // auxname=auxname.replaceAll("\\+", "%2B");
                        //System.out.println(product.getName());
                        p.setName(product.getName());
                        p.setCom(false);
                        p.setScale(false);
                        p.setPriceBuy(product.getPurchasePrice());
                        p.setPriceSell(product.getListPrice());
                        p.setCategoryID(caux.getID());
                        p.setTaxCategoryID(tcaux.getID());
                        //p.setImage(ImageUtils.readImage(product.getImageUrl()));
                        p.setImage(product.getImage());
                        p.setScale("Y".equals(product.getisScale()));
                        // build html display like <html><font size=-2>MIRACLE NOIR<br> MASK</font>
                        p.setDisplay("<html><font size=-2>" + product.getName().substring(0, product.getName().length() > 15 ? 15 : product.getName().length())
                                + "<br>" + ((product.getName().length() > 15) ? product.getName().substring(15, product.getName().length() > 30 ? 30 : product.getName().length()) : "")
                                + "</font>");
                        p.setUpdated(product.getUpdated());
                        
                        
                        p.setService(product.isIsService());
                        dlintegration.syncProduct(p);
                        // Synchronization of stock          
                        if (product instanceof ProductPlus) {
                            ProductPlus productplus = (ProductPlus) product;
                            ProductInfoExt productaux = dlintegration.getProductInfoByReference(productplus.getReference());
                            //  Synchonization of locations
                            
                            dlintegration.syncLocations(productplus.getLocation_id(), productplus.getLocation_name());
                            double diff = productplus.getQtyonhand() - dlintegration.findProductStock(productplus.getLocation_id(), p.getID(), null);
                            if (diff != 0.0) {
                                Object[] diary = new Object[9];
                                diary[0] = UUID.randomUUID().toString();
                                diary[1] = now;
                                diary[2] = diff > 0.0
                                        ? MovementReason.IN_MOVEMENT.getKey()
                                        : MovementReason.OUT_MOVEMENT.getKey();
                                //diary[3] = warehouse;
                                diary[3] = productplus.getLocation_id();
                                String pid = p.getID();
                                if (productaux.getID() != null) {
                                    pid = productaux.getID();
                                }
                                diary[4] = pid;
                                diary[5] = null; ///TODO find out where to get AttributeInstanceID -- red1
                                diary[6] = diff;
                                diary[7] = p.getPriceBuy();
                                diary[8] = dlsystem.getUser();
                                dlintegration.getStockDiaryInsert().exec(diary);
                            }
                        }
                    }

                    //return true;
                }
                if (IsNotSold.compareTo("") == 0) {
                    dlintegration.syncProductsAfter();
                } else {
                    dlintegration.syncProductsAfter(IsNotSold);
                }
            }
        } catch (BasicException ex) {
            Logger.getLogger(SyncProducts.class.getName()).log(Level.SEVERE, null, ex);
        }

        //return false;
    }

    public String getusertoken() {
        try {
            JsonObject jObj = new JsonObject();
            jObj.addProperty("userName", getUserName());
            jObj.addProperty("password", getUserPass());
            URL url = new URL(getUrlBase() + "api/v1/auth/tokens");
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jObj.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);

            String p = jsonObject.get("token").toString();
            in.close();
            return p;
        } catch (Exception e) {
            System.out.println("Error de Autentificacion: " + e.getLocalizedMessage());
        }
        return null;

    }

    public String getclienttoken(String usertoken) {
        try {

            JsonObject jObj = new JsonObject();

            jObj.addProperty("roleId", getAD_Role_ID());
            jObj.addProperty("language", getLanguage());
            jObj.addProperty("clientId", getAD_Client_ID());
            jObj.addProperty("organizationId", getAD_Org_ID());
            jObj.addProperty("warehouseId", getM_Warehouse_ID());

            URL url = new URL(getUrlBase() + "api/v1/auth/tokens");
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + usertoken);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("PUT");

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jObj.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);
            String p = jsonObject.get("token").toString();

            in.close();
            conn.disconnect();
            return p;
        } catch (Exception e) {
            System.out.println("Error de datos de cliente: " + e.getLocalizedMessage());
        }
        return "asd";
    }

    public JSONObject getProductData(String usertoken, int skip) {
        try {
            StringBuilder full_url = new StringBuilder();
            full_url.append(getUrlBase() + "api/v1/models/M_Product_WS_Queue_V?$skip=" + skip + "&");
            //full_url.append("$filter=(WasPrinted eq false;)");
            full_url.append("$filter=" + URLEncoder.encode("M_Warehouse_ID eq " + getM_Warehouse_ID(), "UTF-8"));

            URL url = new URL(full_url.toString());
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) urlConnection;

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + usertoken);
            conn.setDoOutput(false);
            conn.setDoInput(true);
            conn.setRequestMethod("GET");

            /*OutputStream os = urlConnection.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8"); 
		osw.write(param.toString());
		osw.flush();
		osw.close();
		os.close(); */
            conn.connect();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = IOUtils.toString(in, "UTF-8");
            JSONObject jsonObject = new JSONObject(result);
            //System.out.println(jsonObject);
            in.close();
            conn.disconnect();
            return jsonObject;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }

}
